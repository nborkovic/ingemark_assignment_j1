INSERT INTO customer (id, first_name, last_name, email) VALUES (1001, 'Pero', 'Perić', 'email@email.com');
INSERT INTO customer (id, first_name, last_name, email) VALUES (1002, 'Ana', 'Anić', 'email@email.com');

INSERT INTO webshop_order (id, status, total_price_hrk, total_price_eur, customer_id) VALUES (1001, 'DRAFT', 0, 0, 1001);
INSERT INTO webshop_order (id, status, total_price_hrk, total_price_eur, customer_id) VALUES (1002, 'DRAFT', 0, 0, 1001);
INSERT INTO webshop_order (id, status, total_price_hrk, total_price_eur, customer_id) VALUES (1003, 'DRAFT', 0, 0, 1002);


INSERT INTO product (id, code, name, description, price_hrk, is_available) VALUES (1001, '1234567891', 'product1', 'descOfProduct1', 25.00, true);
INSERT INTO product (id, code, name, description, price_hrk, is_available) VALUES (1002, '1234567892', 'product2', 'descOfProduct2', 25.00, true);
INSERT INTO product (id, code, name, description, price_hrk, is_available) VALUES (1003, '1234567893', 'product3', 'descOfProduct3', 25.00, true);
INSERT INTO product (id, code, name, description, price_hrk, is_available) VALUES (1004, '1234567894', 'product4', 'descOfProduct4', 25.00, true);
INSERT INTO product (id, code, name, description, price_hrk, is_available) VALUES (1005, '1234567895', 'product5', 'descOfProduct5', 25.00, true);


INSERT INTO order_item (id, quantity, order_id, product_id) VALUES (1001, 1, 1001, 1001);
INSERT INTO order_item (id, quantity, order_id, product_id) VALUES (1002, 2, 1001, 1002);
INSERT INTO order_item (id, quantity, order_id, product_id) VALUES (1003, 2, 1001, 1003);
INSERT INTO order_item (id, quantity, order_id, product_id) VALUES (1004, 3, 1002, 1004);
INSERT INTO order_item (id, quantity, order_id, product_id) VALUES (1005, 2, 1002, 1005);
INSERT INTO order_item (id, quantity, order_id, product_id) VALUES (1006, 1, 1002, 1001);
INSERT INTO order_item (id, quantity, order_id, product_id) VALUES (1007, 1, 1003, 1002);
INSERT INTO order_item (id, quantity, order_id, product_id) VALUES (1008, 5, 1003, 1003);
INSERT INTO order_item (id, quantity, order_id, product_id) VALUES (1009, 6, 1003, 1004);
INSERT INTO order_item (id, quantity, order_id, product_id) VALUES (10010, 2, 1003, 1005);
