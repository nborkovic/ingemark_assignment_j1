package com.demo.webshop.controller;

import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.math.BigDecimal;
import java.util.HashSet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.demo.webshop.model.Customer;
import com.demo.webshop.model.Order;
import com.demo.webshop.model.OrderItem;
import com.demo.webshop.model.OrderStatus;
import com.demo.webshop.model.Product;
import com.demo.webshop.service.OrderService;
import com.demo.webshop.util.URLInfo;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(OrderController.class)
class OrderControllerTest {

	@Autowired
	MockMvc mockMvc;
	@Autowired
	ObjectMapper mapper;

	@MockBean
	private OrderService orderService;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@BeforeEach
	public void setUp() {
		this.mockMvc = webAppContextSetup(this.webApplicationContext).dispatchOptions(true).build();
	}

	/*****************************************************************************************************************************/
	// /order/read/{id}
	/*****************************************************************************************************************************/

	@Test
	public void getById_existingOrder_successful() throws Exception {

		Long id = 1L;
		Customer existingCustomer = new Customer("FNAME", "LNAME", "EMAIL");
		Order order = new Order(existingCustomer, new HashSet<OrderItem>(), OrderStatus.DRAFT, BigDecimal.ZERO,
				BigDecimal.ZERO);
		order.setId(id);

		Mockito.when(orderService.readById(1L)).thenReturn(order);

		mockMvc.perform(MockMvcRequestBuilders.get("/orders/read/1").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$", notNullValue()));
	}

	@Test
	public void getById_notExistingProduct_fail() throws Exception {

		Mockito.when(orderService.readById(1L)).thenThrow(new Exception("Order doesn't exist!"));

		mockMvc.perform(MockMvcRequestBuilders.get("/orders/read/1").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	/*****************************************************************************************************************************/
	// /product/create
	/*****************************************************************************************************************************/

	@Test
	public void post_regularOrder_successful() throws Exception {
		Long id = 1L;
		Customer existingCustomer = new Customer("FNAME", "LNAME", "EMAIL");
		existingCustomer.setId(id);

		Order expectedOrder = new Order(existingCustomer, new HashSet<OrderItem>(), OrderStatus.DRAFT, BigDecimal.ZERO,
				BigDecimal.ZERO);
		expectedOrder.setId(id);

		Mockito.when(orderService.createNewOrder(Mockito.anyLong())).thenReturn(expectedOrder);

		mockMvc.perform(MockMvcRequestBuilders.get("/orders/create/1")
				.content(mapper.writeValueAsString(existingCustomer)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
	}

	@Test
	public void post_orderForNonExistingCustomer_fail() throws Exception {
		Long id = 1L;
		Customer existingCustomer = new Customer("FNAME", "LNAME", "EMAIL");
		existingCustomer.setId(id);

		Mockito.when(orderService.createNewOrder(Mockito.anyLong()))
				.thenThrow(new Exception("Cannot create order for non existing customer!"));

		mockMvc.perform(MockMvcRequestBuilders.get("/orders/create/1")
				.content(mapper.writeValueAsString(existingCustomer)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	/*****************************************************************************************************************************/
	// /order/addItem/{orderId}
	/*****************************************************************************************************************************/

	@Test
	public void addNewItem_addRegularItem_successful() throws Exception {

		// existingProduct
		Long id = 1L;
		Product existingProduct = new Product("1234567890", "PROD_NAME", BigDecimal.ONE, "PROD_DESC", false);
		existingProduct.setId(id);
		OrderItem item = new OrderItem(existingProduct, 2);
		item.setId(id);

		Mockito.when(orderService.addNewItem(Mockito.anyLong(), Mockito.any(OrderItem.class))).thenReturn(new Order());

		mockMvc.perform(MockMvcRequestBuilders.put("/orders/addItem/1").content(mapper.writeValueAsString(item))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted())
				.andExpect(jsonPath("$", notNullValue()));
	}

	@Test
	public void addNewItem_addItemIfSubbmited_fail() throws Exception {

		// existingProduct
		Long id = 1L;
		Product existingProduct = new Product("1234567890", "PROD_NAME", BigDecimal.ONE, "PROD_DESC", false);
		existingProduct.setId(id);
		OrderItem item = new OrderItem(existingProduct, 2);
		item.setId(id);

		Mockito.when(orderService.addNewItem(Mockito.anyLong(), Mockito.any(OrderItem.class)))
				.thenThrow(new Exception("Cannot add item into the submited order!"));

		mockMvc.perform(MockMvcRequestBuilders.put("/orders/addItem/1").content(mapper.writeValueAsString(item))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	
	/*****************************************************************************************************************************/
	// /order/removeItem/{orderId}
	/**
	 * @throws Exception ***************************************************************************************************************************/
	@Test
	public void removeItem_regularScenario_success() throws Exception {
		// existingProduct
		Long id = 1L;
		Product existingProduct = new Product("1234567890", "PROD_NAME", BigDecimal.ONE, "PROD_DESC", false);
		existingProduct.setId(id);
		OrderItem item = new OrderItem(existingProduct, 2);
		item.setId(id);
		
		Mockito.when(orderService.removeItem(Mockito.anyLong(), Mockito.any(OrderItem.class))).thenReturn(new Order());

		mockMvc.perform(MockMvcRequestBuilders.put("/orders/removeItem/1").content(mapper.writeValueAsString(item))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted())
				.andExpect(jsonPath("$", notNullValue()));
	}
	
	@Test
	public void removeItem_removeItemIfSubbmitedOrder_fail() throws Exception {

		// existingProduct
		Long id = 1L;
		Product existingProduct = new Product("1234567890", "PROD_NAME", BigDecimal.ONE, "PROD_DESC", false);
		existingProduct.setId(id);
		OrderItem item = new OrderItem(existingProduct, 2);
		item.setId(id);

		Mockito.when(orderService.removeItem(Mockito.anyLong(), Mockito.any(OrderItem.class)))
				.thenThrow(new Exception("Cannot remove item from submitted order!"));

		mockMvc.perform(MockMvcRequestBuilders.put("/orders/removeItem/1").content(mapper.writeValueAsString(item))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	

	/*****************************************************************************************************************************/
	// /order/delete/{id}
	/*****************************************************************************************************************************/

	@Test
	public void delete_notExistingOrder_fail() throws Exception {

		Mockito.when(orderService.delete(Mockito.anyLong())).thenThrow(new Exception("Order doesn't exist!"));

		mockMvc.perform(MockMvcRequestBuilders.delete("/orders/delete/16").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest()).andExpect(content().string("Fail: Order doesn't exist!"));
	}

	/*****************************************************************************************************************************/
	// /order/update/{id}
	/*****************************************************************************************************************************/

	@Test
	public void finalize_regularOrder_successful() throws Exception {

		Mockito.when(orderService.finalizeOrder(Mockito.anyLong(), Mockito.any(URLInfo.class))).thenReturn(new Order());

		mockMvc.perform(MockMvcRequestBuilders.get("/orders/finalize/1").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$", notNullValue()));
	}

	@Test
	public void finalize_currencyConversionFail_fail() throws Exception {

		Mockito.when(orderService.finalizeOrder(Mockito.anyLong(), Mockito.any(URLInfo.class)))
				.thenThrow(new Exception("Order finalization failed!"));

		mockMvc.perform(MockMvcRequestBuilders.get("/orders/finalize/1").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("$", notNullValue()));
	}

	@Test
	public void finalize_nonExistingOrder_fail() throws Exception {

		Mockito.when(orderService.finalizeOrder(Mockito.anyLong(), Mockito.any(URLInfo.class)))
				.thenThrow(new Exception("Order doesn't exist!"));

		mockMvc.perform(MockMvcRequestBuilders.get("/orders/finalize/1").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("$", notNullValue()));
	}
}
