package com.demo.webshop.controller;

import java.math.BigDecimal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.demo.webshop.model.Product;
import com.demo.webshop.service.ProductService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import static org.hamcrest.Matchers.*;

@WebMvcTest(ProductController.class)
class ProductControllerTest {

	@Autowired
	MockMvc mockMvc;
	@Autowired
	ObjectMapper mapper;

	@MockBean
	private ProductService productService;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@BeforeEach
	public void setUp() {
		this.mockMvc = webAppContextSetup(this.webApplicationContext).dispatchOptions(true).build();
	}

	/*****************************************************************************************************************************/
	// /product/read/{id}
	/*****************************************************************************************************************************/

	@Test
	public void getById_existingProduct_successful() throws Exception {

		Product product = new Product("CODE", "NAME", BigDecimal.ONE, "DESC", true);
		product.setId(16L);
		Mockito.when(productService.readById(16L)).thenReturn(product);

		mockMvc.perform(MockMvcRequestBuilders.get("/products/read/16").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.code", is("CODE")));
	}

	@Test
	public void getById_notExistingProduct_fail() throws Exception {

		Mockito.when(productService.readById(16L)).thenThrow(new Exception("Product doesn't exist!"));

		mockMvc.perform(MockMvcRequestBuilders.get("/products/read/16").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	/*****************************************************************************************************************************/
	// /product/create
	/*****************************************************************************************************************************/

	@Test
	public void post_regularProduct_successful() throws JsonProcessingException, Exception {
		Product product = new Product("0123456789", "NAME", new BigDecimal(1.00), "DESC", true);
		product.setId(16L);

		Mockito.when(productService.create(product)).thenReturn(product);

		mockMvc.perform(MockMvcRequestBuilders.post("/products/create").content(mapper.writeValueAsString(product))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated());
	}

	@Test
	public void post_tooShortCode_fail() throws JsonProcessingException, Exception {
		Product product = new Product("012345678", "NAME", new BigDecimal(1.00), "DESC", true);
		product.setId(16L);

		Mockito.when(productService.create(Mockito.any(Product.class)))
				.thenThrow(new Exception("Product code must be exactly 10 characters value!"));

		mockMvc.perform(MockMvcRequestBuilders.post("/products/create").content(mapper.writeValueAsString(product))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
				.andExpect(content().string("Fail: Product code must be exactly 10 characters value!"));
	}

	@Test
	public void post_negativePriceValue_fail() throws JsonProcessingException, Exception {
		Product product = new Product("0123456789", "NAME", new BigDecimal(1.00), "DESC", true);
		product.setId(16L);

		Mockito.when(productService.create(Mockito.any(Product.class)))
				.thenThrow(new Exception("Product price must not be negative value!"));

		mockMvc.perform(MockMvcRequestBuilders.post("/products/create").content(mapper.writeValueAsString(product))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
				.andExpect(content().string("Fail: Product price must not be negative value!"));
	}

	@Test
	public void post_productWithExistingCode_fail() throws JsonProcessingException, Exception {
		Product product = new Product("0123456789", "NAME", new BigDecimal(1.00), "DESC", true);
		product.setId(16L);

		Mockito.when(productService.create(Mockito.any(Product.class)))
				.thenThrow(new Exception("Something went wrong! Check if product with the same code already exists."));

		mockMvc.perform(MockMvcRequestBuilders.post("/products/create").content(mapper.writeValueAsString(product))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
				.andExpect(content()
						.string("Fail: Something went wrong! Check if product with the same code already exists."));
	}

	/*****************************************************************************************************************************/
	// /product/update/{id}
	/*****************************************************************************************************************************/

	@Test
	public void update_regularProduct_successful() throws JsonProcessingException, Exception {
		Product product = new Product("0123456789", "NAME", new BigDecimal(1.00), "DESC", true);
		product.setId(16L);

		Mockito.when(productService.update(Mockito.any(Product.class), Mockito.anyLong())).thenReturn(product);

		mockMvc.perform(MockMvcRequestBuilders.put("/products/update/16").content(mapper.writeValueAsString(product))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted());
	}

	@Test
	public void update_nonExistingProduct_successful() throws JsonProcessingException, Exception {
		Product product = new Product("0123456789", "NAME", new BigDecimal(1.00), "DESC", true);
		product.setId(16L);

		Mockito.when(productService.update(Mockito.any(Product.class), Mockito.anyLong()))
				.thenThrow(new Exception("Product doesn't exist!"));

		mockMvc.perform(MockMvcRequestBuilders.put("/products/update/16").content(mapper.writeValueAsString(product))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
				.andExpect(content().string("Fail: Product doesn't exist!"));
	}

	/*****************************************************************************************************************************/
	// /product/update/{id}
	/*****************************************************************************************************************************/

	@Test
	public void delete_existingProduct_successful() throws Exception {

		Product product = new Product("CODE", "NAME", BigDecimal.ONE, "DESC", true);
		product.setId(16L);
		Mockito.when(productService.delete(16L)).thenReturn("Success!");

		mockMvc.perform(MockMvcRequestBuilders.delete("/products/delete/16").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().string("Success!"));
	}


	/*****************************************************************************************************************************/
	// /product/delete/{id}
	/*****************************************************************************************************************************/
	
	@Test
	public void delete_notExistingProduct_fail() throws Exception {

		Mockito.when(productService.delete(Mockito.anyLong())).thenThrow(new Exception("Product doesn't exist!"));

		mockMvc.perform(MockMvcRequestBuilders.delete("/products/delete/16").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest()).andExpect(content().string("Fail: Product doesn't exist!"));
	}

}
