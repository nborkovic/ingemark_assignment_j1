package com.demo.webshop.service;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.demo.webshop.model.Product;
import com.demo.webshop.repository.IProductRepository;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class ProductServiceTest {

	@Mock
	IProductRepository productRepository;

	@InjectMocks
	ProductService productService;

	/*****************************************************************************************************************************/
	// create(product)
	/*****************************************************************************************************************************/

	@Test
	public void create_regularProduct_returnsProduct() throws Exception {
		Product product = new Product("1234567890", "NAME", BigDecimal.ONE, "DESC", true);
		product.setId(16L);

		Mockito.when(productRepository.save(product)).thenReturn(product);

		Product createdProduct = productService.create(product);

		assertThat(createdProduct.getCode()).isEqualTo(product.getCode());
	}

	@Test
	public void create_productWithInvalidCode_ThrowsException() throws Exception {
		Product product = new Product("12345", "NAME", BigDecimal.ONE, "DESC", true);
		assertThrows(Exception.class, () -> {
			productService.create(product);
		});
	}

	@Test
	public void create_productWithInvalidPrice_ThrowsException() throws Exception {
		Product product = new Product("1234567890", "NAME", new BigDecimal(-1), "DESC", true);

		assertThrows(Exception.class, () -> {
			productService.create(product);
		});
	}

	/***************************************************************************************************************************/
	// readById(id)
	/***************************************************************************************************************************/

	@Test
	public void readById_existingProduct_success() throws Exception {
		Long id = 1L;
		Product product = new Product("1234567890", "NAME", BigDecimal.ONE, "DESC", true);
		product.setId(id);

		Mockito.when(productRepository.findById(id)).thenReturn(Optional.of(product));

		Product foundProduct = productService.readById(id);

		assertThat(foundProduct).isEqualTo(product);
	}

	@Test
	public void readById_nonExistingProduct_throwsException() throws Exception {
		Long id = 1L;
		Product product = new Product("1234567890", "NAME", BigDecimal.ONE, "DESC", true);
		product.setId(id);

		Mockito.when(productRepository.findById(id)).thenReturn(Optional.empty());

		assertThrows(Exception.class, () -> {
			productService.readById(id);
		});
	}

	/***************************************************************************************************************************/
	// update(newProduct, id)
	/***************************************************************************************************************************/

	@Test
	public void update_regularProduct_returnsProduct() throws Exception {
		Long id = 1L;

		Product existingProduct = new Product("1234567890", "NAME", BigDecimal.ONE, "DESC", true);
		existingProduct.setId(id);

		
		Product newProduct = new Product("1234567890", "NAME_1", BigDecimal.ONE, "DESC", true);
		newProduct.setId(id);

		Mockito.when(productRepository.findById(id)).thenReturn(Optional.of(existingProduct));
		Mockito.when(productRepository.save(Mockito.any(Product.class))).thenReturn(newProduct);

		Product updatedProduct = productService.update(newProduct, id);

		assertThat(updatedProduct.getName()).isEqualTo("NAME_1");
	}
	
	@Test
	public void update_productWithInvalidCode_throwsException() throws Exception {
		Long id = 1L;

		Product existingProduct = new Product("1234567890", "NAME", BigDecimal.ONE, "DESC", true);
		existingProduct.setId(id);

		
		Product newProduct = new Product("12345", "NAME_1", BigDecimal.ONE, "DESC", true);
		newProduct.setId(id);

		Mockito.when(productRepository.findById(id)).thenReturn(Optional.of(existingProduct));
		Mockito.when(productRepository.save(Mockito.any(Product.class))).thenReturn(newProduct);

		assertThrows(Exception.class, () -> {
			productService.update(newProduct, id);
		});
	}
	
	
	@Test
	public void update_productWithInvalidPrice_throwsException() throws Exception {
		Long id = 1L;

		Product existingProduct = new Product("1234567890", "NAME", BigDecimal.ONE, "DESC", true);
		existingProduct.setId(id);

		
		Product newProduct = new Product("12345", "NAME_1", new BigDecimal(-1), "DESC", true);
		newProduct.setId(id);

		Mockito.when(productRepository.findById(id)).thenReturn(Optional.of(existingProduct));
		Mockito.when(productRepository.save(Mockito.any(Product.class))).thenReturn(newProduct);

		assertThrows(Exception.class, () -> {
			productService.update(newProduct, id);
		});
	}
	
	@Test
	public void update_nonExistingProduct_throwsException() throws Exception {
		Long id = 1L;

		Product existingProduct = new Product("1234567890", "NAME", BigDecimal.ONE, "DESC", true);
		existingProduct.setId(id);

		
		Product newProduct = new Product("12345", "NAME_1", new BigDecimal(-1), "DESC", true);
		newProduct.setId(id);

		Mockito.when(productRepository.findById(id)).thenReturn(Optional.empty());
		Mockito.when(productRepository.save(Mockito.any(Product.class))).thenReturn(newProduct);

		assertThrows(Exception.class, () -> {
			productService.update(newProduct, id);
		});
	}
	
	/***************************************************************************************************************************/
	// delete(id)
	/***************************************************************************************************************************/

	@Test
	public void delete_nonExistingProduct_throwsException() throws Exception {
		Long id = 1L;

		Mockito.when(productRepository.findById(id)).thenReturn(Optional.empty());

		assertThrows(Exception.class, () -> {
			productService.delete(id);
		});		
	}
	

}












