package com.demo.webshop.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.demo.webshop.model.Customer;
import com.demo.webshop.model.Order;
import com.demo.webshop.model.OrderItem;
import com.demo.webshop.model.OrderStatus;
import com.demo.webshop.model.Product;
import com.demo.webshop.repository.IOrderRepository;
import com.demo.webshop.service.OrderService;
import com.demo.webshop.util.URLInfo;
import com.github.tomakehurst.wiremock.WireMockServer;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;

@SpringBootTest
public class WireMockTests {

	WireMockServer wireMockServer = new WireMockServer(8080);
	URLInfo testServerInfo = new URLInfo("http", "localhost", "8080", "/tecajn/v1?valuta=EUR");
	String responseBody = "[{\"Broj tečajnice\":\"176\",\"Datum primjene\":\"12.09.2021\",\"Država\":\"EMU\",\"Šifra valute\":\"978\",\"Valuta\":\"EUR\",\"Jedinica\":1,\"Kupovni za devize\":\"7,456606\",\"Srednji za devize\":\"7,479043\",\"Prodajni za devize\":\"7,501480\"}]";

	@InjectMocks
	OrderService orderService;
	
	@Mock
	IOrderRepository orderRepository;

	
	@BeforeEach
	public void initEach() {
		wireMockServer.start();
		configureFor(testServerInfo.getHost(), Integer.parseInt(testServerInfo.getPort()));
		stubFor(get(urlEqualTo(testServerInfo.getPath())).willReturn(aResponse().withBody(responseBody)));
	}

	@AfterEach
	public void closeEach() {
		wireMockServer.stop();
	}

	@Test
	void fetchExchangeRate_usingSimpleStubbing_thenCorrect() throws IOException {

		BigDecimal result = orderService.fetchExchangeRate(testServerInfo);
		BigDecimal expectedEurExchangeRate = new BigDecimal(7.479043).setScale(7, RoundingMode.HALF_EVEN);

		assertThat(result).isEqualTo(expectedEurExchangeRate);
	}

	@Test
	void calculateTotalPriceEUR_usingSimpleStubbing_thenCorrect() throws IOException {

		BigDecimal result = orderService.calculateTotalPriceEUR(new BigDecimal(100), testServerInfo);
		BigDecimal expectedPriceEUR = new BigDecimal(13.368983).setScale(2, RoundingMode.HALF_EVEN);

		assertThat(result).isEqualTo(expectedPriceEUR);
	}

	@Test
	void finalizeOrder_regularOrder_success() throws Exception {

		// existing order in db
		Long id = 1L;
		Customer existingCustomer = new Customer("FNAME", "LNAME", "EMAIL");
		existingCustomer.setId(id);
		Product product = new Product("1234567890", "PROD_NAME", BigDecimal.ONE, "PROD_DESC", true);
		OrderItem item = new OrderItem(product, 2);
		Set<OrderItem> items = new HashSet<OrderItem>();
		items.add(item);
		Order existingOrder = new Order(existingCustomer, items, OrderStatus.DRAFT, BigDecimal.ZERO, BigDecimal.ZERO);
		existingOrder.setId(id);

		// expected order in db after finalization
		Order expectedSavedOrder = new Order(existingCustomer, items, OrderStatus.SUBMITTED, new BigDecimal(2),
				new BigDecimal(0.27));
		expectedSavedOrder.setId(id);

		Mockito.when(orderRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(existingOrder));
		Mockito.when(orderRepository.save(Mockito.any(Order.class))).thenReturn(expectedSavedOrder);

		assertThat(orderService.finalizeOrder(id, testServerInfo)).isEqualTo(expectedSavedOrder);
	}

}
