package com.demo.webshop.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import com.demo.webshop.model.Customer;
import com.demo.webshop.model.Order;
import com.demo.webshop.model.OrderItem;
import com.demo.webshop.model.OrderStatus;
import com.demo.webshop.model.Product;
import com.demo.webshop.repository.ICustomerRepository;
import com.demo.webshop.repository.IOrderItemRepository;
import com.demo.webshop.repository.IOrderRepository;
import com.demo.webshop.repository.IProductRepository;
import com.demo.webshop.service.OrderService;

@SpringBootTest
public class OrderServiceTest {

	@InjectMocks
	OrderService orderService;

	@Mock
	IOrderRepository orderRepository;

	@Mock
	ICustomerRepository customerRepository;

	@Mock
	IProductRepository productRepository;

	@Mock
	IOrderItemRepository itemRepository;

	/*********************************************************************************************/
	// calculateTotalPriceHRK
	/*********************************************************************************************/

	@Test
	void calculateTotalPriceHRK_regularProductPrices_correctResult() {

		Set<OrderItem> items = new HashSet<OrderItem>();
		for (int i = 0; i < 5; ++i) {
			items.add(new OrderItem(new Product("", "", new BigDecimal(i + 1.5), "", true), 1));
		}

		BigDecimal res = orderService.calculateTotalPriceHRK(items);

		assertThat(res).isEqualTo(new BigDecimal(17.5));
	}

	/*********************************************************************************************/
	// checkAvailabilityOfProducts
	/*********************************************************************************************/

	@Test
	void checkAvailabilityOfProducts_allAvailable_returnsTrue() {

		Set<OrderItem> items = new HashSet<OrderItem>();
		for (int i = 0; i < 5; ++i) {
			items.add(new OrderItem(new Product("", "", BigDecimal.ZERO, "", true), 1));
		}
		Order order = new Order(new Customer(), items, OrderStatus.DRAFT, BigDecimal.ZERO, BigDecimal.ZERO);

		boolean res = orderService.checkAvailabilityOfOrderedProducts(order);

		assertThat(res).isTrue();
	}

	@Test
	void checkAvailabilityOfProducts_oneNotAvailable_returnsFalse() {

		Set<OrderItem> items = new HashSet<OrderItem>();
		for (int i = 0; i < 5; ++i) {
			items.add(new OrderItem(new Product("", "", BigDecimal.ZERO, "", true), 1));
		}
		items.add(new OrderItem(new Product("", "", BigDecimal.ZERO, "", false), 1));

		Order order = new Order(new Customer(), items, OrderStatus.DRAFT, BigDecimal.ZERO, BigDecimal.ZERO);

		boolean res = orderService.checkAvailabilityOfOrderedProducts(order);

		assertThat(res).isFalse();
	}

	@Test
	void checkAvailabilityOfProducts_allUnavailable_returnsFalse() {

		Set<OrderItem> items = new HashSet<OrderItem>();
		for (int i = 0; i < 5; ++i) {
			items.add(new OrderItem(new Product("", "", BigDecimal.ZERO, "", false), 1));
		}

		Order order = new Order(new Customer(), items, OrderStatus.DRAFT, BigDecimal.ZERO, BigDecimal.ZERO);

		boolean res = orderService.checkAvailabilityOfOrderedProducts(order);

		assertThat(res).isFalse();
	}

	/*****************************************************************************************************************************/
	// create(customer)
	/*****************************************************************************************************************************/

	@Test
	public void create_existingCustomer_success() throws Exception {
		Long customerId = 1L;
		Long orderId = 1L;

		Customer existingCustomer = new Customer("FNAME", "LNAME", "EMAIL");
		existingCustomer.setId(customerId);
		Order newOrder = new Order(existingCustomer, new HashSet<OrderItem>(), OrderStatus.DRAFT, BigDecimal.ZERO,
				BigDecimal.ZERO);
		newOrder.setId(orderId);

		Mockito.when(customerRepository.findById(customerId)).thenReturn(Optional.of(existingCustomer));
		Mockito.when(orderRepository.save(Mockito.any(Order.class))).thenReturn(newOrder);

		assertThat(orderService.createNewOrder(existingCustomer.getId())).isEqualTo(newOrder);
	}

	@Test
	public void create_nonExistingCustomer_success() throws Exception {
		Long customerId = 1L;
		Customer nonExistingCustomer = new Customer("FNAME", "LNAME", "EMAIL");
		nonExistingCustomer.setId(customerId);

		Mockito.when(customerRepository.findById(customerId)).thenReturn(Optional.empty());

		assertThrows(Exception.class, () -> {
			orderService.createNewOrder(nonExistingCustomer.getId());
		});
	}

	/***************************************************************************************************************************/
	// readById(id)
	/***************************************************************************************************************************/

	@Test
	public void readById_existingOrder_success() throws Exception {
		Long id = 1L;
		Customer existingCustomer = new Customer("FNAME", "LNAME", "EMAIL");
		Order order = new Order(existingCustomer, new HashSet<OrderItem>(), OrderStatus.DRAFT, BigDecimal.ZERO,
				BigDecimal.ZERO);
		order.setId(id);

		Mockito.when(orderRepository.findById(id)).thenReturn(Optional.of(order));

		Order foundOrder = orderService.readById(id);

		assertThat(foundOrder).isEqualTo(order);
	}

	@Test
	public void readById_nonExistingOrder_throwsException() throws Exception {
		Long id = 1L;
		Customer existingCustomer = new Customer("FNAME", "LNAME", "EMAIL");
		Order order = new Order(existingCustomer, new HashSet<OrderItem>(), OrderStatus.DRAFT, BigDecimal.ZERO,
				BigDecimal.ZERO);
		order.setId(id);

		Mockito.when(orderRepository.findById(id)).thenReturn(Optional.empty());

		assertThrows(Exception.class, () -> {
			orderService.readById(id);
		});
	}

	/***************************************************************************************************************************/
	// addNewItem(orderId, orderedItem)
	/***************************************************************************************************************************/

	@Test
	public void addNewItem_regularScenario_success() throws Exception {

		// existing order in db
		Long id = 1L;
		Customer existingCustomer = new Customer("FNAME", "LNAME", "EMAIL");
		existingCustomer.setId(id);
		Order existingOrder = new Order(existingCustomer, new HashSet<OrderItem>(), OrderStatus.DRAFT, BigDecimal.ZERO,
				BigDecimal.ZERO);
		existingOrder.setId(id);

		// existingProduct
		Product existingProduct = new Product("1234567890", "PROD_NAME", BigDecimal.ONE, "PROD_DESC", true);
		existingProduct.setId(id);
		OrderItem item = new OrderItem(existingProduct, 2);
		item.setId(id);
		Set<OrderItem> items = new HashSet<OrderItem>();
		items.add(item);

		// expected order in db
		Order expectedSavedOrder = new Order(existingCustomer, items, OrderStatus.DRAFT, BigDecimal.ZERO,
				BigDecimal.ZERO);
		expectedSavedOrder.setId(id);

		Mockito.when(orderRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(existingOrder));
		Mockito.when(productRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(existingProduct));
		Mockito.when(itemRepository.save(Mockito.any(OrderItem.class))).thenReturn(item);
		Mockito.when(orderRepository.save(Mockito.any(Order.class))).thenReturn(expectedSavedOrder);

		assertThat(orderService.addNewItem(id, item)).isEqualTo(expectedSavedOrder);
	}

	@Test
	public void addNewItem_notAvailableProduct_throwsException() throws Exception {

		// existing order in db
		Long id = 1L;
		Customer existingCustomer = new Customer("FNAME", "LNAME", "EMAIL");
		existingCustomer.setId(id);
		Order existingOrder = new Order(existingCustomer, new HashSet<OrderItem>(), OrderStatus.DRAFT, BigDecimal.ZERO,
				BigDecimal.ZERO);
		existingOrder.setId(id);

		// existingProduct
		Product existingProduct = new Product("1234567890", "PROD_NAME", BigDecimal.ONE, "PROD_DESC", false);
		existingProduct.setId(id);
		OrderItem item = new OrderItem(existingProduct, 2);
		item.setId(id);
		Set<OrderItem> items = new HashSet<OrderItem>();
		items.add(item);

		// expected order in db
		Order expectedSavedOrder = new Order(existingCustomer, items, OrderStatus.DRAFT, BigDecimal.ZERO,
				BigDecimal.ZERO);
		expectedSavedOrder.setId(id);

		Mockito.when(orderRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(existingOrder));
		Mockito.when(productRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(existingProduct));
		Mockito.when(itemRepository.save(Mockito.any(OrderItem.class))).thenReturn(item);
		Mockito.when(orderRepository.save(Mockito.any(Order.class))).thenReturn(expectedSavedOrder);

		assertThrows(Exception.class, () -> {
			orderService.addNewItem(id, item);
		});
	}

	@Test
	public void addNewItem_nonExistingProduct_throwsException() throws Exception {

		// existing order in db
		Long id = 1L;
		Customer existingCustomer = new Customer("FNAME", "LNAME", "EMAIL");
		existingCustomer.setId(id);
		Order existingOrder = new Order(existingCustomer, new HashSet<OrderItem>(), OrderStatus.DRAFT, BigDecimal.ZERO,
				BigDecimal.ZERO);
		existingOrder.setId(id);

		// existingProduct
		Product existingProduct = new Product("1234567890", "PROD_NAME", BigDecimal.ONE, "PROD_DESC", false);
		existingProduct.setId(id);
		OrderItem item = new OrderItem(existingProduct, 2);
		item.setId(id);
		Set<OrderItem> items = new HashSet<OrderItem>();
		items.add(item);

		// expected order in db
		Order expectedSavedOrder = new Order(existingCustomer, items, OrderStatus.DRAFT, BigDecimal.ZERO,
				BigDecimal.ZERO);
		expectedSavedOrder.setId(id);

		Mockito.when(orderRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(existingOrder));
		Mockito.when(productRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		Mockito.when(itemRepository.save(Mockito.any(OrderItem.class))).thenReturn(item);
		Mockito.when(orderRepository.save(Mockito.any(Order.class))).thenReturn(expectedSavedOrder);

		assertThrows(Exception.class, () -> {
			orderService.addNewItem(id, item);
		});
	}

	@Test
	public void addNewItem_nonExistingOrder_throwsException() throws Exception {

		// existing order in db
		Long id = 1L;
		Customer existingCustomer = new Customer("FNAME", "LNAME", "EMAIL");
		existingCustomer.setId(id);
		Order existingOrder = new Order(existingCustomer, new HashSet<OrderItem>(), OrderStatus.DRAFT, BigDecimal.ZERO,
				BigDecimal.ZERO);
		existingOrder.setId(id);

		// existingProduct
		Product existingProduct = new Product("1234567890", "PROD_NAME", BigDecimal.ONE, "PROD_DESC", false);
		existingProduct.setId(id);
		OrderItem item = new OrderItem(existingProduct, 2);
		item.setId(id);
		Set<OrderItem> items = new HashSet<OrderItem>();
		items.add(item);

		// expected order in db
		Order expectedSavedOrder = new Order(existingCustomer, items, OrderStatus.DRAFT, BigDecimal.ZERO,
				BigDecimal.ZERO);
		expectedSavedOrder.setId(id);

		Mockito.when(orderRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		Mockito.when(productRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(existingProduct));
		Mockito.when(itemRepository.save(Mockito.any(OrderItem.class))).thenReturn(item);
		Mockito.when(orderRepository.save(Mockito.any(Order.class))).thenReturn(expectedSavedOrder);

		assertThrows(Exception.class, () -> {
			orderService.addNewItem(id, item);
		});
	}

	@Test
	public void addNewItem_submittedOrder_throwsException() throws Exception {

		// existing order in db
		Long id = 1L;
		Customer existingCustomer = new Customer("FNAME", "LNAME", "EMAIL");
		existingCustomer.setId(id);
		Order existingOrder = new Order(existingCustomer, new HashSet<OrderItem>(), OrderStatus.SUBMITTED,
				BigDecimal.ZERO, BigDecimal.ZERO);
		existingOrder.setId(id);

		// existingProduct
		Product existingProduct = new Product("1234567890", "PROD_NAME", BigDecimal.ONE, "PROD_DESC", false);
		existingProduct.setId(id);
		OrderItem item = new OrderItem(existingProduct, 2);
		item.setId(id);
		Set<OrderItem> items = new HashSet<OrderItem>();
		items.add(item);

		// expected order in db
		Order expectedSavedOrder = new Order(existingCustomer, items, OrderStatus.DRAFT, BigDecimal.ZERO,
				BigDecimal.ZERO);
		expectedSavedOrder.setId(id);

		Mockito.when(orderRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(existingOrder));
		Mockito.when(productRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(existingProduct));
		Mockito.when(itemRepository.save(Mockito.any(OrderItem.class))).thenReturn(item);
		Mockito.when(orderRepository.save(Mockito.any(Order.class))).thenReturn(expectedSavedOrder);

		assertThrows(Exception.class, () -> {
			orderService.addNewItem(id, item);
		});
	}

	/***************************************************************************************************************************/
	// removeItem(orderId, orderedItem)
	/***************************************************************************************************************************/

	@Test
	public void removeItem_nonExistingOrder_throwsException() {

		Long id = 1L;
		// existingProduct
		Product existingProduct = new Product("1234567890", "PROD_NAME", BigDecimal.ONE, "PROD_DESC", false);
		existingProduct.setId(id);
		OrderItem item = new OrderItem(existingProduct, 2);
		item.setId(id);

		Mockito.when(orderRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		
		assertThrows(Exception.class, () -> {
			orderService.addNewItem(id, item);
		});
	}


	@Test
	public void removeItem_submittedOrder_throwsException() {

		Long id = 1L;
		// existingProduct
		Product existingProduct = new Product("1234567890", "PROD_NAME", BigDecimal.ONE, "PROD_DESC", false);
		existingProduct.setId(id);
		OrderItem item = new OrderItem(existingProduct, 2);
		item.setId(id);
		
		Customer existingCustomer = new Customer("FNAME", "LNAME", "EMAIL");
		existingCustomer.setId(id);
		Order existingOrder = new Order(existingCustomer, new HashSet<OrderItem>(), OrderStatus.SUBMITTED,
				BigDecimal.ZERO, BigDecimal.ZERO);
		existingOrder.setId(id);
		
		

		Mockito.when(orderRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(existingOrder));
		
		assertThrows(Exception.class, () -> {
			orderService.addNewItem(id, item);
		});
	}

	/***************************************************************************************************************************/
	// delete(id)
	/***************************************************************************************************************************/

	@Test
	public void delete_nonExistingProduct_throwsException() throws Exception {
		Long id = 1L;

		Mockito.when(orderRepository.findById(id)).thenReturn(Optional.empty());

		assertThrows(Exception.class, () -> {
			orderService.delete(id);
		});
	}

}
