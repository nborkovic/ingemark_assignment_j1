package com.demo.webshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.webshop.model.Product;
import com.demo.webshop.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private ProductService productService;

	@GetMapping("/read/{id}")
	public ResponseEntity<?> getById(@PathVariable Long id) {
		try {
			Product foundProduct = productService.readById(id);
			return new ResponseEntity<Product>(foundProduct, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Fail: " + e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/create")
	public ResponseEntity<?> post(@RequestBody Product newProduct) {
		try {
			return new ResponseEntity<Product>(productService.create(newProduct), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<String>("Fail: " + e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<?> update(@RequestBody Product product, @PathVariable Long id) {
		try {
			return new ResponseEntity<Product>(productService.update(product, id), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<String>("Fail: " + e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> delete(@PathVariable Long id) {
		try {
			productService.delete(id);
			return new ResponseEntity<String>("Success!", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Fail: " + e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
