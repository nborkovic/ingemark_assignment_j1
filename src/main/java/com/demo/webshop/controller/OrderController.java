package com.demo.webshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.webshop.model.Order;
import com.demo.webshop.model.OrderItem;
import com.demo.webshop.service.OrderService;
import com.demo.webshop.util.URLInfo;

@RestController
@RequestMapping("/orders")
public class OrderController {

	@Autowired
	OrderService orderService;

	URLInfo hnbUrlInfo = new URLInfo("https", "api.hnb.hr", "", "/tecajn/v1?valuta=EUR");

	@GetMapping("/read/{id}")
	public ResponseEntity<?> getById(@PathVariable Long id) {
		try {
			Order foundOrder = orderService.readById(id);
			return new ResponseEntity<Order>(foundOrder, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Fail: " + e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/create/{customerId}")
	public ResponseEntity<?> post(@PathVariable Long customerId) {
		try {
			return new ResponseEntity<Order>(orderService.createNewOrder(customerId), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<String>("Fail: " + e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("/addItem/{orderId}")
	public ResponseEntity<?> addNewItem(@RequestBody OrderItem orderedItem, @PathVariable Long orderId) {
		try {
			return new ResponseEntity<Order>(orderService.addNewItem(orderId, orderedItem), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<String>("Fail: " + e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("/removeItem/{orderId}")
	public ResponseEntity<?> removeItem(@RequestBody OrderItem orderedItem, @PathVariable Long orderId) {
		try {
			return new ResponseEntity<Order>(orderService.removeItem(orderId, orderedItem), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<String>("Fail: " + e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> delete(@PathVariable Long id) {
		try {
			orderService.delete(id);
			return new ResponseEntity<String>("Success!", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Fail: " + e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/finalize/{id}")
	public ResponseEntity<?> finalize(@PathVariable Long id) {
		try {
			Order finalizedOrder = orderService.finalizeOrder(id, this.hnbUrlInfo);
			return new ResponseEntity<Order>(finalizedOrder, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Fail: " + e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
