package com.demo.webshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.webshop.model.OrderItem;

@Repository
public interface IOrderItemRepository extends JpaRepository<OrderItem, Long> {

}
