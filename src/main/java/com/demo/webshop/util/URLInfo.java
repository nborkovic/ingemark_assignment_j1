package com.demo.webshop.util;

public class URLInfo {

	private String protocol;
	private String host;
	private String port;
	private String path;
	
	
	public URLInfo(String protocol, String host, String port, String path) {
		super();
		this.protocol = protocol;
		this.host = host;
		this.port = port;
		this.path = path;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}	
	
	
	public String getUrl() {
		if (this.getPort().equals("")) {
			return this.getProtocol() + "://" + this.getHost() + this.getPath();
		}else {
			return this.getProtocol() + "://" + this.getHost() + ":" + this.getPort() + this.getPath();
		}
	}
}
