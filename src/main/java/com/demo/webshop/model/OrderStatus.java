package com.demo.webshop.model;

public enum OrderStatus {

	DRAFT, SUBMITTED

}