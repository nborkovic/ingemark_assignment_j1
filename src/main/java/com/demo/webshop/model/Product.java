package com.demo.webshop.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;

@Entity
@Table(name = "product")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "code", unique = true, length = 10)
	private String code;

	@Column(name = "name")
	private String name;

	// https://www.baeldung.com/javax-ovalidation
	@Column(name = "price_hrk")
	@DecimalMin("0.00")
	private BigDecimal priceHRK;

	@Column(name = "description")
	private String description;

	@Column(name = "is_available")
	private boolean isAvailable;

	public Product() {
		super();
	}

	public Product(String code, String name, @DecimalMin("0.00") BigDecimal priceHRK, String description,
			boolean isAvailable) {
		super();
		this.code = code;
		this.name = name;
		this.priceHRK = priceHRK;
		this.description = description;
		this.isAvailable = isAvailable;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPriceHRK() {
		return priceHRK;
	}

	public void setPriceHRK(BigDecimal priceHRK) {
		this.priceHRK = priceHRK;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	public boolean hasValidCode() {
		return this.getCode().length() == 10;
	}

	public boolean hasValidPrice() {
		return this.getPriceHRK().compareTo(BigDecimal.ZERO) >= 0;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", code=" + code + ", name=" + name + ", priceHRK=" + priceHRK + ", description="
				+ description + ", isAvailable=" + isAvailable + "]";
	}
}
