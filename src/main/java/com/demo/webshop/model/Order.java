package com.demo.webshop.model;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "webshop_order")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne(optional = false, cascade = { CascadeType.MERGE })
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer customer;

	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<OrderItem> items;

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private OrderStatus status;

	@Column(name = "total_price_hrk")
	private BigDecimal totalPriceHRK;

	@Column(name = "total_price_eur")
	private BigDecimal totalPriceEUR;

	public Order() {
		super();
	}

	public Order(Customer customer, Set<OrderItem> items, OrderStatus status, BigDecimal totalPriceHRK,
			BigDecimal totalPriceEUR) {
		super();
		this.customer = customer;
		this.items = items;
		this.status = status;
		this.totalPriceHRK = totalPriceHRK;
		this.totalPriceEUR = totalPriceEUR;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Set<OrderItem> getItems() {
		return items;
	}

	public void setItems(Set<OrderItem> items) {
		this.items = items;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public BigDecimal getTotalPriceHRK() {
		return this.totalPriceHRK;
	}

	public void setTotalPriceHRK(BigDecimal totalPriceHRK) {
		this.totalPriceHRK = totalPriceHRK;
	}

	public BigDecimal getTotalPriceEUR() {
		return totalPriceEUR;
	}

	public void setTotalPriceEUR(BigDecimal totalPriceEUR) {
		this.totalPriceEUR = totalPriceEUR;
	}

	public boolean addNewItem(OrderItem newItem) {
		return this.items.add(newItem);
	}

	public boolean removeItem(OrderItem item) {
		return this.items.remove(item);
	}

	@Override
	public String toString() {
		String orderInfo = "Order [id=" + id + ", customer=" + customer + ", status=" + status + ", totalPriceHRK="
				+ totalPriceHRK + ", totalPriceEUR=" + totalPriceEUR + "]\nItems:\n";
		for (OrderItem item : this.getItems()) {
			orderInfo += "   " + item.toString() + "\n";
		}
		return orderInfo;
	}
}
