package com.demo.webshop.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.webshop.model.Customer;
import com.demo.webshop.model.Order;
import com.demo.webshop.model.OrderItem;
import com.demo.webshop.model.OrderStatus;
import com.demo.webshop.model.Product;
import com.demo.webshop.repository.ICustomerRepository;
import com.demo.webshop.repository.IOrderItemRepository;
import com.demo.webshop.repository.IOrderRepository;
import com.demo.webshop.repository.IProductRepository;
import com.demo.webshop.util.URLInfo;

@Service
public class OrderService {

	@Autowired
	IOrderRepository orderRepository;

	@Autowired
	IProductRepository productRepository;

	@Autowired
	IOrderItemRepository itemRepository;

	@Autowired
	ICustomerRepository customerRepository;

	/**
	 * Creates new order for customer and saves it to the database.
	 * 
	 * @param customerId
	 * @return Created order
	 * @throws Exception if customer with given id doesn't exist.
	 */
	public Order createNewOrder(Long customerId) throws Exception {
		if (customerRepository.findById(customerId).isPresent()) {
			Customer customer = customerRepository.findById(customerId).get();
			return orderRepository.save(
					new Order(customer, new HashSet<OrderItem>(), OrderStatus.DRAFT, BigDecimal.ZERO, BigDecimal.ZERO));
		} else {
			throw new Exception("Cannot create order for non existing customer!");
		}
	}

	/**
	 * Reads the order with given id from the database.
	 * 
	 * @param id
	 * @return order from database
	 * @throws Exception if order doesn't exist.
	 */
	public Order readById(Long id) throws Exception {
		if (orderRepository.findById(id).isPresent()) {
			return orderRepository.findById(id).get();
		} else {
			throw new Exception("Order doesn't exist!");
		}
	}

	/**
	 * Adds new item to the order
	 * 
	 * @param orderId
	 * @param orderedItem
	 * @return Updated order
	 * @throws Exception if the order does not exist, the order has already been
	 *                   submitted, or the product of the order item is not
	 *                   available or does not exist.
	 */
	public Order addNewItem(Long orderId, OrderItem orderedItem) throws Exception {
		if (orderRepository.findById(orderId).isPresent()) {
			Order existingOrder = orderRepository.findById(orderId).get();

			if (existingOrder.getStatus().equals(OrderStatus.SUBMITTED)) {
				throw new Exception("Cannot add item into the submitted order!");
			}

			if (productRepository.findById(orderedItem.getProduct().getId()).isPresent()) {
				Product product = productRepository.findById(orderedItem.getProduct().getId()).get();
				if (!orderedItem.getProduct().getIsAvailable()) {
					throw new Exception("Product: " + product.getName() + " is not available!");
				}
				orderedItem.setProduct(product);
				orderedItem.setOrder(existingOrder);
				OrderItem savedItem = itemRepository.save(orderedItem);
				existingOrder.addNewItem(savedItem);
				return orderRepository.save(existingOrder);
			} else {
				throw new Exception("Product with id: " + orderedItem.getProduct().getId() + " does not exist!");
			}
		} else {
			throw new Exception("Order with id: " + orderId + " does not exist!");
		}
	}

	/**
	 * Removes item from the order
	 * 
	 * @param orderId
	 * @param orderedItem
	 * @return updated order
	 * @throws Exception if the order has already been submitted or doesn't exist.
	 */
	public Order removeItem(Long orderId, OrderItem orderedItem) throws Exception {

		if (orderRepository.findById(orderId).isPresent()) {
			Order existingOrder = orderRepository.findById(orderId).get();

			if (existingOrder.getStatus().equals(OrderStatus.SUBMITTED)) {
				throw new Exception("Cannot remove item from submitted order!");
			}
			itemRepository.deleteById(orderedItem.getId());

			return orderRepository.findById(orderId).get();
		} else {
			throw new Exception("Order with id: " + orderId + " does not exist!");
		}
	}

	/**
	 * Finalizes order. Method sets the proper values of total prices and submits
	 * the order.
	 * 
	 * @param id
	 * @param hnbUrlInfo
	 * @return finalized order
	 * @throws Exception if the order has already been submitted or doesn't exist.
	 */
	public Order finalizeOrder(Long id, URLInfo hnbUrlInfo) throws Exception {
		if (orderRepository.findById(id).isPresent()) {
			Order existingOrder = orderRepository.findById(id).get();

			if (existingOrder.getStatus().equals(OrderStatus.SUBMITTED)) {
				throw new Exception("Cannot finalize submitted order!");
			}

			try {
				existingOrder.setTotalPriceHRK(calculateTotalPriceHRK(existingOrder.getItems()));
				existingOrder.setTotalPriceEUR(calculateTotalPriceEUR(existingOrder.getTotalPriceHRK(), hnbUrlInfo));
				existingOrder.setStatus(OrderStatus.SUBMITTED);
				return orderRepository.save(existingOrder);
			} catch (Exception e) {
				throw new Exception("Order finalization failed!");
			}
		} else {
			throw new Exception("Order doesn't exist!");
		}
	}

	/**
	 * Deletes the order all order items.
	 * 
	 * @param orderId
	 * @return Message of success.
	 * @throws Exception if order doesn't exist
	 */
	public String delete(Long id) throws Exception {
		if (orderRepository.findById(id).isPresent()) {
			orderRepository.deleteById(id);
			return "Success!";
		} else {
			throw new Exception("Order doesn't exist!");
		}
	}

	/**
	 * Calculates the total price in HRK
	 * 
	 * @param items
	 * @return total price in HRK
	 */
	public BigDecimal calculateTotalPriceHRK(Collection<OrderItem> items) {
		BigDecimal totalPriceHrk = BigDecimal.ZERO;
		for (OrderItem item : items) {
			totalPriceHrk = totalPriceHrk
					.add(item.getProduct().getPriceHRK().multiply(BigDecimal.valueOf(item.getQuantity())));
		}
		return totalPriceHrk;
	}

	/**
	 * Calculate total price in EUR from the total price in HRK
	 * 
	 * @param totalPriceHRK
	 * @param hnbUrlInfo
	 * @return total price in EUR
	 * @throws IOException if currency conversion didn't succeed.
	 */
	public BigDecimal calculateTotalPriceEUR(BigDecimal totalPriceHRK, URLInfo hnbUrlInfo) throws IOException {
		BigDecimal exchangeRate = fetchExchangeRate(hnbUrlInfo);
		return totalPriceHRK.divide(exchangeRate, 2, RoundingMode.HALF_EVEN);
	}

	/**
	 * Fetches the exchange rate from HNB API using
	 * 
	 * @param hnbUrlInfo
	 * @return EUR exchange rate
	 * @throws IOException if fetching failed.
	 */
	public BigDecimal fetchExchangeRate(URLInfo hnbUrlInfo) throws IOException {
		URL completeUrl = new URL(hnbUrlInfo.getUrl());

		HttpURLConnection con = (HttpURLConnection) completeUrl.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/json");
		con.setDoOutput(true);

		try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
			StringBuilder response = new StringBuilder();
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}

			JSONArray rootArray = new JSONArray(response.toString());
			JSONObject rootObject = rootArray.optJSONObject(0);
			String exchangeRate = rootObject.getString("Srednji za devize");
			exchangeRate = exchangeRate.replaceAll(",", ".");
			return new BigDecimal(exchangeRate).setScale(7, RoundingMode.HALF_EVEN);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Checks availability of products in given order.
	 * 
	 * @param order
	 * @return true if all products are available, false if some of the products are
	 *         not available.
	 */
	public boolean checkAvailabilityOfOrderedProducts(Order order) {
		for (OrderItem item : order.getItems()) {
			if (!item.getProduct().getIsAvailable()) {
				return false;
			}
		}
		return true;
	}
}
