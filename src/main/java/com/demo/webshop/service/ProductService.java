package com.demo.webshop.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.webshop.model.Product;
import com.demo.webshop.repository.IProductRepository;

@Service
public class ProductService
//implements ISimpleCrudService<Product> 
{

	@Autowired
	IProductRepository productRepository;

	/**
	 * Creates new product
	 * @param newProduct
	 * @return created product
	 * @throws Exception if values are not valid or product with the same code already exists.
	 */
	public Product create(Product newProduct) throws Exception {

		if (!newProduct.hasValidCode())
			throw new Exception("Product code must be exactly 10 characters value!");
		if (!newProduct.hasValidPrice())
			throw new Exception("Product price must not be negative value!");
		try {
			return productRepository.save(newProduct);
		} catch (Exception e) {
			throw new Exception("Something went wrong! Check if product with the same code already exists.");
		}
	}

	/**
	 * Returns the product with given id from the database.
	 * @param id
	 * @return product
	 * @throws Exception id product doesn't exist in database.
	 */
	public Product readById(Long id) throws Exception {
		Optional<Product> optProduct = productRepository.findById(id);

		if (optProduct.isPresent()) {
			return productRepository.findById(id).get();
		} else {
			throw new Exception("Product doesn't exist!");
		}
	}

	/**
	 * Updates values of given product in the database.
	 * @param newProduct
	 * @param id
	 * @return updated product
	 * @throws Exception
	 */
	public Product update(Product newProduct, Long id) throws Exception {

		if (productRepository.findById(id).isPresent()) {
			Product product = productRepository.findById(id).get();
			product.setCode(newProduct.getCode());
			product.setName(newProduct.getName());
			product.setPriceHRK(newProduct.getPriceHRK());
			product.setDescription(newProduct.getDescription());
			product.setIsAvailable(newProduct.getIsAvailable());
			return create(product);
		} else {
			throw new Exception("Product doesn't exist!");
		}
	}

	/**
	 * Deletes product from the database
	 * @param id
	 * @return Success message
	 * @throws Exception if the product is still part of some order item.
	 */
	public String delete(Long id) throws Exception {

		if (productRepository.findById(id).isPresent()) {
			try {
				productRepository.deleteById(id);
			} catch (Exception e) {
				throw new Exception("Cannot delete product: " +id + ". Maybe it is still part of some order item!");
			}
		
			return "Success!";
		} else {
			throw new Exception("Product doesn't exist!");
		}
	}
}
