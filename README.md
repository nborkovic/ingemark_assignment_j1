# INGEMARK assignment J1

The main goal of this assignment is to create a REST webshop backend service using the following technology stack:
- Java 8+
- Maven
- Git
- Spring Boot + Spring Web MVC
- Spring Data JPA
- Relational database (use Postgres in Docker container)
- JUnit , AssertJ , Wiremock for unit tests

## Installation
Set up local environment and start the server.
### Setup PostgreSQL database
PostgreSQL DBMS can be set up locally or in docker container. 
Install postgres and set up the database with following initial properties:
- Host: ```localhost```
- Port: ```5432 ```
- Database name: ```webshop_db```
- username: ```postgres```
- password: ```123```

> Note: You can change database initial properties, but make sure that you change it in `application.properties` too.

### Build and run the Spring Boot application
- Clone project from this repo and enter the project directory:
```sh
git clone https://gitlab.com/nborkovic/ingemark_assignment_j1.git
```
```sh
cd ingemark_assignment_j1/
```
- You can first import the project into your favourite (Java) IDE, or just run it using Maven
```sh
./mvnw spring-boot:run
```
> Note: When you run application for the first time, database tables will be created automatically. If you want to prepopulate tables with initial dataset, you can use the `dataset.sql` script placed in this repo.

- Run unit tests
```sh
./mvnw test
```
- Start using application.


## API Documentation

Before usage read the [REST API Documentation](https://documenter.getpostman.com/view/5722180/U16opPZ6).
